#pragma once

class Player
{
public:
	float tx, ty, tz;
	float speed;

	Player()
	{
		tx = 0;
		ty = 0;
		tz = 0;
		speed = 0;
	}

	void Move(float tx, float ty, float tz);
};
