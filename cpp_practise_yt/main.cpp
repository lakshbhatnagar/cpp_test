#include <iostream>
#include "Log.h"
#include "AddLog.h"
#include "Player.h"
#include <string>
#include "nvFileIO/nvFileRW.h"

#define string std::string

int main()
{
	readFile("testFile");
	
	Player my_player;

	string test = "Hello World";

	std::cout << test << std::endl;
	
	float playerSpeed = my_player.speed;

	playerSpeed = 15.0f;

	my_player.Move(0.3f, 12.4f, 15.5f);

	std::cout << playerSpeed << std::endl;
}